sudo mkdir workspace
sudo apt install  git -y
wget https://www.python.org/ftp/python/3.9.1.tgz
tar -Xf Python-3.9.1.tgz
sudo apt-get update
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/$
 echo \
 "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://d$
 $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

